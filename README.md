jquery.currencies.js

 Не имею ни малейшего понятия, работает ли это, к сожалению.

## Installation

Download jquery.currencies.min.js and upload it to your Shopify theme assets.
Include the file in your theme.liquid file __before the closing body tag__ like so:

    {{ "/services/javascripts/currencies.js" | script_tag }}
    {{ "jquery.currencies.min.js" | asset_url | script_tag }}

## Usage

TO START WITH:
   let Currencies = $().currencyExt();

To save a picked currency to a cookie, use the following line of code, passing the currency code as parameter:

    Currencies.LocStor_write('CAD');

To read the currency code saved to your 'currencies' cookie, use the following code:

    var storedCurrency = Currencies.LocStor_read();

The above will return the currency code, or will return null if the cookie does not exist.

To convert formatted money (with or without the currency code and descriptor) to formatted money in another currency use this:

    Currencies.convertAll(oldCurrency, newCurrency, selector, format);

The parameters _oldCurrency_ and _newCurrency_ must be set to the 3-letter currency codes of the FROM and TO currencies.

The parameter _selector_ is a CSS selector that tells the function where to find the money on the page. It is optional. If it is not used, the function will look for all span elements on the page with a class attribute set to 'money', and will convert the money in those elements.

So, using it without _selector_ is the same as calling the function like so:

    Currencies.convertAll('CAD', 'USD', 'span.money');

The parameter __format__ is optional and can take on the value 'money_format' or 'money_with_currency_format'.

Calling the function without _format_ is the same as calling the function like so:

    Currencies.convertAll('CAD', 'USD', 'span.money', 'money_with_currency_format');


## Optional global settings

Add the following line of JavaScript before you own code, if you do not want the formatted money to show the currency descriptor:

    Currencies.change_format('money_format');

If you don't use the above line of code, the formatted money will be showing both the currency symbol and descriptor, i.e. it will show $20.00 USD instead of $20.00.

If you want to use a different name for your cookie, use this:

    Currencies.LocStor_ch_name('my_awesome_name');

If you don't use the above line of code, the name of your cookie will be 'currencies'.
